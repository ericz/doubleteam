var bridge;
var photoService;
var photos;
var cur;

$(function(){
  
  bridge = new Bridge({apiKey:'bbbbbbbb'}).connect();

  bridge.joinChannel('photo', {

    insert: function(doc){
      photos[doc._id] = doc;
      $('#links').append(createBox(doc));
    },
    
    updateTitle: function(id, title){
      photos[id].title = title;
      if(cur._id === id) {
        setTitle(title);
      }
    },
    
    updatePhoto: function(id, zone, name){
      photos[id][zone] = name;
      if(cur._id === id) {
        setImage(zone, name);
      }
      setThumb(id, zone, name);
    },
    
    remove: function(id){
      $('#'+id).remove();
      delete photos[id];
      if(cur._id === id) {
        setCur(photos[Object.keys(photos).pop()]);
      }
    }
    
  });

  bridge.getService('photo', function(service){
    photoService = service;
    photoService.get(function(d){
      photos = {};
      var links = $('#links');
      for(var i = 0, ii = d.length; i < ii; i++) {
        var photo = d[i];
        photos[photo._id] = photo;
        var box = createBox(photo);
        links.append(box);
      }
      setCur(d[i-1]);
    });
  });

  function createBox(photo){    
    var box = $('<div></div>').addClass('box');
    var left = $('<div></div>').addClass('pane').attr('id', photo._id+'z1').css('background-image', 'url(/images/' + photo.z1 + ')').appendTo(box);
    var right = $('<div></div>').addClass('pane').attr('id', photo._id+'z2').css('background-image', 'url(/images/' + photo.z2 + ')').appendTo(box);
    var border = $('<div></div>').addClass('border').attr('id', photo._id).css('-webkit-transform', 'rotate('+(Math.floor(Math.random()*40)-20)+'deg)').click(function(){
      setCur(photos[$(this).attr('id')]);
    }).append(box);
    return border;
  }
  
  function setCur(photo) {
    cur = photo;
    setTitle(photo.title);
    setImage('z1', photo.z1);
    setImage('z2', photo.z2);
  }
  
  function setImage(zone, name) {
    $('#'+zone).css('background-image', 'url(/images/' + name + ')');
  };
  
  function setThumb(id, zone, name) {
    $('#'+id+zone).css('background-image', 'url(/images/' + name + ')');
  };
  
  function setTitle(title) {
    $('#title').text(title);
  };
  
  var dropbox = $("#z1, #z2").on("dragenter", dragenter).on("dragover", dragover).on("drop", drop);  

  function dragenter(e) {  
    e.stopPropagation();  
    e.preventDefault();  
  }  
    
  function dragover(e) {  
    e.stopPropagation();  
    e.preventDefault();  
  }  

  function drop(e) {  
    e.stopPropagation();  
    e.preventDefault();  
    var file = e.originalEvent.dataTransfer.files[0];  
    photoService.updatePhoto(cur._id, $(this).attr('id'), file, '.' + file.name.split('.').pop());
  }
  
  $('#title').editable(function(value){
    photoService.updateTitle(cur._id, value);
  });

  $('#add').click(function(){
    photoService.insert();
  });
  
  $('#main').css('height', $(window).outerHeight() - 300);
  
  $('#delete').click(function(){
    photoService.remove(cur._id);
  });
});