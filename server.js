var Bridge = require('bridge');
var express = require('express');
var fs = require('fs');
var mongo = require('mongoskin');
var db = mongo.db('localhost:27017/doubleteam').collection('photos');

var app =  express.createServer();

var photoChannel;


// Initialize main server
app.use(express.bodyParser());

app.use(express.static(__dirname + '/public'));

app.set('view engine', 'ejs');
app.set('views', __dirname + '/views');


app.get('/', function(req, res){
  res.render('index');
});


app.listen(80);


var bridge = new Bridge({apiKey:'bbbbbbbb'}).connect();


bridge.joinChannel('photo', {
  insert: function(){},
  updateTitle: function(){},
  updatePhoto: function(){},
  remove: function(){}
}, function(channel){
  photoChannel = channel;
});

bridge.publishService('photo', {
  
  insert: function(){
    db.insert({title: 'New Photo', z1: 'blank.png', z2: 'blank.png'}, function(err, docs){
      docs[0]._id = docs[0]._id.toString();
      photoChannel.insert(docs[0]);
    });
  }, 
  
  updateTitle: function(id, title) {
    db.updateById(id, {$set: {title: title}});
    photoChannel.updateTitle(id, title);
  },
  
  updatePhoto: function(id, zone, photo, ext){
    var name = id + zone + ext;
    photo.on('end', function(data){ 
      var newPhotos = {};
      newPhotos[zone] = name;
      db.updateById(id, {$set: newPhotos});   
      fs.open(__dirname + '/public/images/' + name, 'w', function(err, fd){    
        fs.write(fd, data, 0, data.length, 0, function(){
          photoChannel.updatePhoto(id, zone, name);
        });
      });
    });
  },
  
  remove: function(id) {
    db.removeById(id);
    photoChannel.remove(id);
  },
  
  get: function(cb){
    db.find().toArray(function(err, docs){
      for (var i = 0, ii = docs.length; i < ii; i++) {
        docs[i]._id = docs[i]._id.toString();
      }
      cb(docs);
    });
  }
  
});